package Question1;


abstract class Fruit {
    int unitPrice;
    int amount;
    double disCount;

    public abstract double actualCost(int unitPrice,int amount,double discount);

    public abstract void fruitDetail();

}
