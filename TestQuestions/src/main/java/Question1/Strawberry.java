package Question1;

public class Strawberry extends Fruit{

    public Strawberry(int unitPrice,int amount,double discount){
        this.unitPrice = unitPrice;
        this.amount = amount;
        this.disCount = discount;
    }

    @Override
    public double actualCost(int unitPrice, int amount, double discount) {
        return unitPrice * amount * discount;
    }

    @Override
    public void fruitDetail() {
        System.out.println("水果类型：草莓，单价："+unitPrice + ",重量："+amount + ",折扣：" + disCount);
    }
}
