package Question1;


public class Test {
    public static void main(String[] args) {
        Fruit[] fruits1 = {new Apple(8,10,1),new Strawberry(13,5,1)};
        System.out.println("A水果总价：" + sum(fruits1));

        Fruit[] fruits2 = {new Apple(8,10,1),new Strawberry(13,5,1),new Mango(20,3,1)};
        System.out.println("B水果总价：" +sum(fruits2));

        Fruit[] fruits3 = {new Apple(8,10,1),new Strawberry(13,5,0.8),new Mango(20,3,1)};
        System.out.println("C水果总价：" + sum(fruits3));
    }

    static double sum(Fruit[] fruits){
        double sum = 0;
        for(int i = 0;i < fruits.length; i++){
            sum += fruits[i].actualCost(fruits[i].unitPrice,fruits[i].amount,fruits[i].disCount);
        }
        return sum;
    }
}
